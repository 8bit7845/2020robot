/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ElevatorConstants;
public class Elevator extends SubsystemBase {

  CANSparkMax m_ElevatorMaster = new CANSparkMax(ElevatorConstants.kElevator1, MotorType.kBrushless);
  CANSparkMax m_ElevatorSlave = new CANSparkMax(ElevatorConstants.kElevator2, MotorType.kBrushless);
  WPI_VictorSPX m_ElevatorOpen  = new WPI_VictorSPX(ElevatorConstants.kElevatorUp);





  public Elevator() {
    m_ElevatorMaster.restoreFactoryDefaults();
    m_ElevatorSlave.restoreFactoryDefaults();
    m_ElevatorSlave.follow(m_ElevatorMaster, true);
    m_ElevatorOpen.setInverted(true);
    stopElevator();
    //pump.set(false);
  }
  public void open(){
    m_ElevatorOpen.set(0.3);
  }
  public void stopOpen(){
    m_ElevatorOpen.set(0);
  }
  public void  up(){
    m_ElevatorMaster.set(0.3);
  }
  public void down(){
    m_ElevatorMaster.set(-1);
  }
  public void stopElevator(){
    m_ElevatorMaster.set(0);
  }
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

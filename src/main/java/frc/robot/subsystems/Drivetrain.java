/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import java.util.function.DoubleSupplier;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DriveConstants;

public class DriveTrain extends SubsystemBase {
  /**
   * Creates a new DriveTrain.
   */
  CANSparkMax m_leftMaster = new CANSparkMax(DriveConstants.kLeftMotor1Port, MotorType.kBrushless);
  CANSparkMax m_leftSlave = new CANSparkMax(DriveConstants.kLeftMotor2Port, MotorType.kBrushless);
  CANSparkMax m_rightMaster = new CANSparkMax(DriveConstants.kRightMotor1Port, MotorType.kBrushless);
  CANSparkMax m_rightSlave = new CANSparkMax(DriveConstants.kRightMotor2Port, MotorType.kBrushless);

  DifferentialDrive m_train = new DifferentialDrive(m_leftMaster, m_rightMaster);

  public DriveTrain() {
    m_rightMaster.restoreFactoryDefaults();
    m_leftMaster.restoreFactoryDefaults();

    m_leftMaster.setInverted(false);
    m_rightMaster.setInverted(false);

    m_leftSlave.follow(m_leftMaster, false);
    m_rightSlave.follow(m_rightMaster, false);
    switchModes(IdleMode.kCoast);

    Leftencoder.setPosition(0);
    Rightencoder.setPosition(0);
  
  }

  CANEncoder Leftencoder = new CANEncoder(m_leftMaster);
  CANEncoder Rightencoder = new CANEncoder(m_rightMaster);

  public void drive(double moveValue, double rotateValue){
      m_train.curvatureDrive(moveValue, rotateValue, true);
  }

  
  /**
  *Sets the max output of the drive.  Useful for scaling the drive to drive more slowly.
  *
  * @param maxOutput the maximum output to which the drive will be constrained
  */



  public void setMaxOutput(double maxOutput) {
    m_train.setMaxOutput(maxOutput);
  }
  
  public void setMaxOutput(DoubleSupplier maxOutput) {
    m_train.setMaxOutput(maxOutput.getAsDouble());
  }

  public void rampRate(double rate){
    m_leftMaster.setOpenLoopRampRate(rate);
    m_rightMaster.setOpenLoopRampRate(rate);
  }

  public void switchModes(IdleMode mode) {
    m_leftMaster.setIdleMode(mode);
    m_leftSlave.setIdleMode(mode);
    m_rightMaster.setIdleMode(mode);
    m_rightSlave.setIdleMode(mode);
  }


  public void stop(){
    SmartDashboard.putString("stop", "stop");
    m_leftMaster.set(0);
    m_leftSlave.set(0);
    m_rightMaster.set(0);
    m_rightSlave.set(0);
  }

  public void test(){
    m_leftMaster.set(0.2);
    Timer.delay(0.25);
    m_leftMaster.set(0.0);
    m_rightMaster.set(0.2);
    Timer.delay(0.25);
    m_rightMaster.set(0);
    stop();
  }
  public void resetEncoderpos(){
    Leftencoder.setPosition(0);
    Rightencoder.setPosition(0);
  }
  public double getRightEncoderPos(){
    return Rightencoder.getPosition();
  }
  public double getLeftEncoderPos(){
    return -Leftencoder.getPosition();
  }

  public double encodersPos(){
    return (((-Leftencoder.getPosition()) + (Rightencoder.getPosition())) / 2) / 0.2;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("m_leftMaster speed", m_leftMaster.get());
    SmartDashboard.putNumber("Leftencoder Position", -Leftencoder.getPosition());
    SmartDashboard.putNumber("Rightencoder Position", Rightencoder.getPosition());
  }
}

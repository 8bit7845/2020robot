/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.Rev2mDistanceSensor;
import com.revrobotics.Rev2mDistanceSensor.Port;
import com.revrobotics.Rev2mDistanceSensor.Unit;

import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.BallConstants;
import frc.robot.commands.smartConveyor;
import jdk.jshell.ErroneousSnippet;

public class BallIntake extends SubsystemBase {
  /**
   * Creates a new BallIntake.
   */
  WPI_VictorSPX m_verticalBelt = new WPI_VictorSPX(BallConstants.kVerticalBelt);
  WPI_VictorSPX m_horizontalBelt = new WPI_VictorSPX(BallConstants.kHorizontalBelt);

  WPI_VictorSPX m_beltWheels = new WPI_VictorSPX(BallConstants.kBeltWheels);
  WPI_VictorSPX m_pickupWheels = new WPI_VictorSPX(BallConstants.kPickWheels);

  CANSparkMax m_shooterMaster = new CANSparkMax(BallConstants.kShooter1, MotorType.kBrushless);
  CANSparkMax m_shooterSlave = new CANSparkMax(BallConstants.kShooter2, MotorType.kBrushless);
  CANEncoder m_encoder_shooterMaster = m_shooterMaster.getEncoder();

 

  Rev2mDistanceSensor distSens = new Rev2mDistanceSensor(Port.kOnboard);

  DutyCycleEncoder throughBore = new DutyCycleEncoder(3);

  double timeNow = 0;
  double timePast = 0;
  double shooterRPM = 0;
  public double error;
  public double targetRPM;
  public final double maxRPM = 6500;

  public BallIntake() {
    m_shooterMaster.setInverted(true);
    m_shooterSlave.follow(m_shooterMaster);
    
    m_verticalBelt.setInverted(true);
    m_horizontalBelt.setInverted(true);

    m_beltWheels.setInverted(false);

    throughBore.reset();

    distSens.setAutomaticMode(true);
  }

  public void stop(){ //stops everything
    conveyorStop();
    m_shooterMaster.set(0.0);
    m_beltWheels.set(0.0);
    BallCollectorStop();
  }

  public void in() {
    m_verticalBelt.set(1);
    m_horizontalBelt.set(0.8);
  }

  public void in(double speed) {
    if (speed!=0) {
      m_verticalBelt.set(speed + 0.15);
      m_horizontalBelt.set(speed);
    }
    else {
      m_verticalBelt.set(0);
      m_horizontalBelt.set(0);
    }
  }

  public void out() {
    m_verticalBelt.set(-0.4);
    m_horizontalBelt.set(-0.4);
  }

  public void out(double speed) {
    m_verticalBelt.set(speed);
    m_horizontalBelt.set(speed);
  }
  public void conveyorStop() {
    m_verticalBelt.set(0);
    m_horizontalBelt.set(0);
  }
  public void openLoopShoot() {
    m_shooterMaster.set(1.0);
  }

  public void openLoopShoot(double speed) {
    m_shooterMaster.set(speed);
  }

  public void BallCollector() {
    m_beltWheels.set(0.4);
    m_pickupWheels.set(0.8);
  }

  public void BallCollectorStop() {
    m_beltWheels.set(0.0);
    m_pickupWheels.set(0.0);
  }

  public boolean ballOccupied(){
    return distSens.getRange(Unit.kMillimeters) < 80;
  }
  public double getRewSpeed(){
    return m_shooterMaster.get();
  }
  public void shootRPM(double targetRPM){
    this.targetRPM = targetRPM;
    double kP = 0.0001;
    SmartDashboard.putNumber("p error", error*kP);

    m_shooterMaster.set((targetRPM/maxRPM)-error*kP);
  }
  public boolean RPMonTarget(){
    if(error<250 && error>-250){ SmartDashboard.putBoolean("setspeed", true); return true;}
    else                       { SmartDashboard.putBoolean("setspeed", false); return false;}
  }


  public double getRPM(){
    timeNow = Timer.getFPGATimestamp();
    if (timeNow != 0 && timePast != 0) shooterRPM = ( throughBore.get() / ((timeNow - timePast) / 60));
    throughBore.reset();
    timePast = timeNow;
    return shooterRPM;
  }

  public double getShootSpeed(){
    return m_shooterMaster.get();
  }

  public void test(){ //tests everything
    m_verticalBelt.set(0.2);
    m_horizontalBelt.set(0.2);
    Timer.delay(0.25);
    m_verticalBelt.set(0.0);
    m_horizontalBelt.set(0.0);
    m_shooterMaster.set(0.2);
    m_beltWheels.set(0.2);
    m_pickupWheels.set(0.2);
    Timer.delay(0.25);
    m_shooterMaster.set(0.0);
    m_beltWheels.set(0.0);
    m_pickupWheels.set(0.0);
  }

  
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    double x= getRPM();
    error = (-1*targetRPM) + x;
    SmartDashboard.putNumber("Flywheel errror", error);
    SmartDashboard.putNumber("Flywheel RPM", x);

    SmartDashboard.putNumber("balldist", distSens.getRange(Unit.kMillimeters));
    SmartDashboard.putBoolean("ball coming in", ballOccupied());
    SmartDashboard.putNumber("rew speed", m_shooterMaster.get());
  }
}

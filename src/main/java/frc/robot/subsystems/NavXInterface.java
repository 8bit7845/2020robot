/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import com.kauailabs.navx.frc.AHRS;

public class NavXInterface extends SubsystemBase {
  /**
   * Creates a new NavXInterface.
   */
  AHRS ahrs;

  public NavXInterface() {
    try {
      ahrs = new AHRS(SPI.Port.kMXP);
      ahrs.enableLogging(true);
      ahrs.reset();
    } catch (RuntimeException ex) {
      DriverStation.reportError("Error instantiating navX MXP:  " + ex.getMessage(), true);
    }
  }

  public float getPitch() {
    return ahrs.getPitch();
  }

  public float getYaw() {
    return ahrs.getYaw();
  }

  public void yewReset() {
    ahrs.reset();
  }

  public float getRoll() {
    return ahrs.getRoll();
  }

  public double getAngle() {
    return ahrs.getAngle();
  }

  public double getAngleAfterAddition(double degrees) {
    return ahrs.getAngle() + degrees;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("Gyro Yaw", getYaw()); 
    SmartDashboard.putNumber("Gyro Roll", getRoll()); 
    SmartDashboard.putNumber("Gyro Pitch", getPitch());
    SmartDashboard.putNumber("Gyro Angle", getAngle());
  }
}

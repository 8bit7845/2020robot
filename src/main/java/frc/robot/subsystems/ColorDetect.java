/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.util.Color;
import frc.robot.Constants.ColorConstants;
import frc.robot.Constants.BallConstants;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;
import edu.wpi.first.wpilibj.I2C;

public class ColorDetect extends SubsystemBase {
  
 
  
  private ColorSensorV3 se_colorSensor = new ColorSensorV3(I2C.Port.kOnboard);
  private ColorMatch m_colorMatcher = new ColorMatch();
   
  String colorString;
  ColorMatchResult match;
  private WPI_VictorSPX  m_colorSensor = new WPI_VictorSPX(BallConstants.kColorRotate);
  int COUNTER;
  String LASTCOLOR;

  /**
   * Creates a new ColorDetect.
   */
  public ColorDetect() { 
    m_colorMatcher.addColorMatch(ColorConstants.kBlueTarget);
    m_colorMatcher.addColorMatch(ColorConstants.kGreenTarget);
    m_colorMatcher.addColorMatch(ColorConstants.kRedTarget);
    m_colorMatcher.addColorMatch(ColorConstants.kYellowTarget);
  }
  public void outputColor(){
    match = m_colorMatcher.matchClosestColor(se_colorSensor.getColor());
    if (match.color == ColorConstants.kBlueTarget) {
      colorString = "Blue";
    } else if (match.color == ColorConstants.kRedTarget) {
      colorString = "Red";
    } else if (match.color == ColorConstants.kGreenTarget) {
      colorString = "Green";
    } else if (match.color ==ColorConstants.kYellowTarget) {
      colorString = "Yellow";
    } else {
      colorString = "Unknown";
    }
  }
  public boolean colorFound(Color destColor){
    if(match.color == destColor) return true; 
    else return false;
  }
  public void stopMotor(){
    m_colorSensor.set(0);
  }
  public void rotateUntilCorrect(){
    m_colorSensor.set(0.5);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import java.lang.Math;

public class LimelightInterface extends SubsystemBase {
  /**
   * Creates a new limelightinterface.
   */
  NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
  NetworkTableEntry tx = table.getEntry("tx");
  NetworkTableEntry ty = table.getEntry("ty");
  NetworkTableEntry ta = table.getEntry("ta");
  NetworkTableEntry tv = table.getEntry("tv");
  NetworkTableEntry ts = table.getEntry("ts");
  public NetworkTableEntry pipeline = table.getEntry("pipeline");
  public NetworkTableEntry led = table.getEntry("ledMode");

  double x = 0.0;
  double y = 0.0;
  double area = 0.0;
  boolean hasTarget = false;

  public LimelightInterface() {
    switchPipeline(0);
  }

  public double getDistance(){
    double h = 212-40, a1 = 24.5, a2 = getY();
    return h/Math.tan(a1+a2)+10;
  }

  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }

  public double getArea() {
    return area;
  }

  /*public double getRotation() {
    return distance;
  }*/

  public boolean hasValidTarget() {
    return hasTarget;
  }
  
  public void switchPipeline(int pipeNum){
    //pipeline.setNumber(pipeNum);
    pipeline.setNumber(0);
  }

      /**
     * returs the distance from the the target [tan(a1+a2) = (h2-h1) / d]
     * @param the angel of the target
     * @return the distance in meters
     */

  
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    //read values periodically
    x = tx.getDouble(0.0);
    y = ty.getDouble(0.0);
    area = ta.getDouble(0.0);
    hasTarget = tv.getDouble(0.0) == 1.0;
    SmartDashboard.putBoolean("LimelightTarget", hasTarget);  
    SmartDashboard.putNumber("x", x);    
    SmartDashboard.putNumber("y", y);  
    SmartDashboard.putNumber("dist", getDistance());  
  }
}

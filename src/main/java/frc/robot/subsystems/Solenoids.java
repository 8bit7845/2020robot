/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.SolenoidConstants;

public class Solenoids extends SubsystemBase {
  /**
   * Creates a new Solenoids.
   */
  Solenoid pumpLock = new Solenoid(SolenoidConstants.kLock); 

  Solenoid pumpBallCollector = new Solenoid(SolenoidConstants.kBall1); // pump.set(true/false);
  

  public Solenoids() {
    pumpLock.set(false);
    pumpBallCollector.set(false);
  }

  public void openElevator(){
    pumpLock.set(false);
  }
  
  public void closeElevator(){
    pumpLock.set(true);
  }

  public void openBallCollector(){
    pumpBallCollector.set(true);
  }

  public void closeBallCollector(){
    pumpBallCollector.set(false);
  }

  public void lockElevator(boolean lock){
    pumpLock.set(!lock);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

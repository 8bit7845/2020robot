/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.LimelightInterface;


public class AutoShoot extends CommandBase {
  /**
   * Creates a new AutoShoot.
   */
  private final BallIntake s_ballintake;
  private final LimelightInterface s_limelightinterface;
  private double RPM ;
  double maxRPM = 6500;
  boolean onTargetRPM;


  public AutoShoot(LimelightInterface limelightinterface, BallIntake ballIntake, double RPM) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_ballintake = ballIntake;
    s_limelightinterface = limelightinterface;
    addRequirements(s_limelightinterface);
    addRequirements(s_ballintake);
    this.RPM = RPM;
  }
  

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    s_ballintake.shootRPM(RPM);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    s_ballintake.shootRPM(RPM);
    Timer.delay(0.5);
    if(s_ballintake.RPMonTarget()) s_ballintake.in();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    s_ballintake.stop();
    s_ballintake.RPMonTarget();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }

}

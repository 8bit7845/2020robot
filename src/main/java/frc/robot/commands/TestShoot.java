/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.LimelightInterface;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class TestShoot extends CommandBase {
  /**
   * Creates a new TestShoot.
   */
  final double kpTurn = 0.01;
  final double kpRange = -0.05;
  final double min_command = 0.05;
  double turnError;
  double rangeError;
  double speedAdjust = -1.0;
  private final BallIntake s_ballintake;
  private final LimelightInterface s_limelightinterface;
  public TestShoot(LimelightInterface limelightinterface, BallIntake ballIntake) {
    s_ballintake = ballIntake;
    s_limelightinterface = limelightinterface;
    // Use addRequirements() here to declare subsystem dependencies.
    // Configure additional PID options by calling `getController` here.
    addRequirements(s_ballintake);
    addRequirements(s_limelightinterface);
    SmartDashboard.putNumber("RealShootSpeed", 0);
  }

  @Override
  public void execute() {
      s_ballintake.openLoopShoot(SmartDashboard.getNumber("RealShootSpeed", s_ballintake.getShootSpeed()));
      s_ballintake.in();
  }

  @Override
  public void end(boolean interrupted) {
    s_ballintake.stop();
  }
  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

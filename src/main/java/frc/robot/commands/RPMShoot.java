/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.LimelightInterface;
import frc.robot.util.getRPMForDistance;;

public class RPMShoot extends CommandBase {
  /**
   * Creates a new RPMShoot.
   */
  private final BallIntake s_ballintake;
  private final LimelightInterface s_limelightinterface;
  private final getRPMForDistance s_getRPMForDistance;
  double maxRPM = 6500;
  boolean onTargetRPM;
  public RPMShoot(LimelightInterface limelightinterface, BallIntake ballIntake, getRPMForDistance getRPMForDistance) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_ballintake = ballIntake;
    s_limelightinterface = limelightinterface;
    s_getRPMForDistance = getRPMForDistance;
    addRequirements(s_limelightinterface);
    addRequirements(s_ballintake);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //s_ballintake.shootRPM(setRPM());
    s_ballintake.shootRPM(5000);
    Timer.delay(0.5);
    if(s_ballintake.RPMonTarget()) s_ballintake.in();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    s_ballintake.stop();
    s_ballintake.RPMonTarget();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
  public double setRPM(){
    double rpm;
    rpm = s_getRPMForDistance.get(s_limelightinterface.getY());
    return rpm;
  }
}

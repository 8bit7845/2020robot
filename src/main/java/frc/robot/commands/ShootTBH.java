/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import frc.robot.util.TBHCommand;
import frc.robot.util.TBHController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.LimelightInterface;
import frc.robot.util.getRPMForDistance;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class ShootTBH extends TBHCommand {
  /**
   * Creates a new ShootTBH.
   */
  private final LimelightInterface s_limelightinterface;
  private final BallIntake s_ballintake;
  public ShootTBH(LimelightInterface limelightInterface, BallIntake ballIntake) {
    super(
        // The controller that the command will use
        new TBHController(),
        // This should return the measurement
        () -> ballIntake.getRPM(),
        // This should return the setpoint (can also be a constant)
        //() -> getRPMForDistance.get(limelightInterface.getDistance()),
        4000,
        // This uses the output
        output -> {
          ballIntake.openLoopShoot(-output);
          SmartDashboard.putNumber("attempt at ballintaking", output);
        });
    // Use addRequirements() here to declare subsystem dependencies.
    // Configure additional PID options by calling `getController` here.
    s_limelightinterface = limelightInterface;
    s_ballintake = ballIntake;
    s_limelightinterface.switchPipeline(0);
  }

  @Override
  public void end(boolean interrupted) {
    s_limelightinterface.switchPipeline(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return getController().isOnTarget();
  }
}

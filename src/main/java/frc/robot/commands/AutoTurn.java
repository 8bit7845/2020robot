/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.PIDCommand;

import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.NavXInterface;

public class AutoTurn extends PIDCommand {
  /**
   * Creates a new outo.
   */
  private final DriveTrain s_drivetrain;
  private final NavXInterface s_navxinterface;

  private boolean isFirstRun = true;
  private static double turnValue = 0;

  public AutoTurn(NavXInterface navxInterface, DriveTrain driveTrain, double degrees) {
    super(
        // The controller that the command will use
        new PIDController(0.00426, 0, 0),
        // This should return the measurement
        () -> navxInterface.getAngle(),
        // This should return the setpoint (can also be a constant)
        () -> degrees,
        // This uses the output
        output -> {
          // Use the output here
          driveTrain.drive(0, ((output<0) ? (output-0.04) : (output+0.04)));
          turnValue = output;
        });
    // Use addRequirements() here to declare subsystem dependencies.
    s_drivetrain = driveTrain;
    s_navxinterface = navxInterface;
    addRequirements(s_navxinterface);
    addRequirements(s_drivetrain);
    this.getController().setTolerance(1,2);
  }

  @Override
  public void execute() {
    if (isFirstRun) {
      m_useOutput.accept(m_controller.calculate(m_measurement.getAsDouble(),
                                                m_setpoint.getAsDouble()+m_measurement.getAsDouble()));
      isFirstRun = false;
    }
    else m_useOutput.accept(m_controller.calculate(m_measurement.getAsDouble()));
    SmartDashboard.putNumber("turnError", getController().getPositionError());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    s_drivetrain.stop();
    isFirstRun = true;
  }
  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return this.getController().atSetpoint();
  }
}
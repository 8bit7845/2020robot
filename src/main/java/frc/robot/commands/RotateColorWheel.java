/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ColorConstants;
import frc.robot.subsystems.ColorDetect;


public class RotateColorWheel extends CommandBase {

  private final ColorDetect s_colordetect;

  /**
   * Creates a new RotateColorWheel.
   */
  public RotateColorWheel(ColorDetect s_colordetect) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.s_colordetect = s_colordetect;
    addRequirements(s_colordetect);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }
  
  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    s_colordetect.rotateUntilCorrect();
    s_colordetect.outputColor();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    s_colordetect.stopMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return s_colordetect.colorFound(ColorConstants.kBlueTarget);
  }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Solenoids;

public class smartConveyor extends CommandBase {
  /**
   * Creates a new smartConveyor.
   */
  private final BallIntake s_ballintake;
  private final DriveTrain s_drivetrain;
  private final Solenoids s_solenoid;

  private final Joystick Xbox = new Joystick(0);
  
  public smartConveyor(BallIntake ballintake, DriveTrain drivetrain, Solenoids solenoid) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_ballintake = ballintake;
    s_drivetrain = drivetrain;
    s_solenoid = solenoid;
    addRequirements(s_solenoid);
    addRequirements(s_ballintake);
    //addRequirements(s_drivetrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    s_drivetrain.setMaxOutput(1);
    s_ballintake.BallCollector();
    s_solenoid.openBallCollector();
    if(s_ballintake.ballOccupied()){
      s_ballintake.in(0.4); // set the speed. it affects the space betweeen balls
      Xbox.setRumble(RumbleType.kRightRumble, 1);  
      }  
    else s_ballintake.in(0);
  }

  // Called once the command ends or is interrupted.3

  @Override
  public void end(boolean interrupted) {
    s_drivetrain.setMaxOutput(0.8);
    s_ballintake.BallCollectorStop();
    s_solenoid.closeBallCollector();
    s_ballintake.conveyorStop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

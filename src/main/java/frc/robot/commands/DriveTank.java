/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveTank extends CommandBase {
  /**
   * Creates a new DriveTank.
   */
  private final DriveTrain s_drivetrain;
  private final DoubleSupplier v_forward;
  private final DoubleSupplier v_rotation;
  private final DoubleSupplier v_limit;

  public DriveTank(DriveTrain driveTrain, DoubleSupplier forward, DoubleSupplier rotation, DoubleSupplier limit) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_drivetrain = driveTrain;
    v_forward = forward;
    v_rotation = rotation;
    v_limit = limit;
    addRequirements(s_drivetrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    s_drivetrain.drive(-1*v_forward.getAsDouble()*v_limit.getAsDouble(), -1*v_rotation.getAsDouble()*v_limit.getAsDouble()); //INVER THE AXSIS
    //s_drivetrain.drive(v_forward.getAsDouble()*v_limit.getAsDouble(), v_rotation.getAsDouble()*v_limit.getAsDouble());
  }

  // Called once the command ends or is interrupted.
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

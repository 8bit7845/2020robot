/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.LimelightInterface;
import frc.robot.util.EndTimerCommand;

public class AutoAlignTurn extends CommandBase {
  /**
   * Creates a new AutoAlignTurn.
   */
  private final DriveTrain s_drivetrain;
  private final LimelightInterface s_limelightinterface;
  private final BallIntake s_baBallIntake;
  final double kpTurn = 0.005;
  final double kpRange = -0.05;
  final double min_command = 0.04;
  double turnError;
  double rangeError;
  double distanceOffset;
  double Ydistans;
  boolean distance;
  double speedAdjust = -1.0;
  double turningAdjust;
  double distentAdjust;
  NetworkTableEntry pipeline;

  public AutoAlignTurn(BallIntake ballIntake, LimelightInterface limelightinterface, DriveTrain driveTrain) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_drivetrain = driveTrain;
    s_limelightinterface = limelightinterface;
    s_baBallIntake = ballIntake;
    addRequirements(s_limelightinterface);
    addRequirements(s_drivetrain);
    distance = false;
  }

  public AutoAlignTurn(BallIntake ballIntake, LimelightInterface limelightinterface, DriveTrain driveTrain, double distanceOffset) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_drivetrain = driveTrain;
    s_limelightinterface = limelightinterface;
    s_baBallIntake = ballIntake;
    addRequirements(s_limelightinterface);
    addRequirements(s_drivetrain);
    this.distanceOffset = distanceOffset;
    distance= true;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    s_drivetrain.setMaxOutput(0.8);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    s_limelightinterface.switchPipeline(0);
    //Timer.delay(0.5);
    if(!s_limelightinterface.hasValidTarget()) s_drivetrain.drive(0, 0.08);
    else 
    {
      //s_baBallIntake.openLoopShoot(-0.7); // starts the shoot to save time
      turnError = -s_limelightinterface.getX();
      rangeError = -s_limelightinterface.getY();
      turningAdjust = 0;
      
      if(s_limelightinterface.getX()>1) turningAdjust = kpTurn * turnError - min_command;
      else if (s_limelightinterface.getX()<1) turningAdjust = kpTurn * turnError + min_command;

//      s_drivetrain.drive(0, -turningAdjust);
//      SmartDashboard.putNumber("turningAdjust", -turningAdjust);

      if (!distance) s_drivetrain.drive(0, -turningAdjust);
      else
      {
        distentAdjust = (distanceOffset + rangeError) * kpRange;
        s_drivetrain.drive(distentAdjust, -turningAdjust);
      }
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    //new EndTimerCommand(new InstantCommand(() -> s_limelightinterface.switchPipeline(1)), 5);
    s_drivetrain.drive(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
if(!distance){
  if (turnError>-2 &&  turnError<2 && s_limelightinterface.hasValidTarget())
  {
    Timer.delay(0.5);
    if(turnError>-2 &&  turnError<2 && s_limelightinterface.hasValidTarget())
          {return true;}
    else  {return false;}
  } 
  else
  return false;}
else{
  if (turnError>-2 &&  turnError<2 &&(rangeError+distanceOffset)>-2 &&  (rangeError+distanceOffset)<2 && s_limelightinterface.hasValidTarget())
  {
    Timer.delay(0.5);
    if(turnError>-2 &&  turnError<2 &&(rangeError+distanceOffset)>-2 &&  (rangeError+distanceOffset)<2 && s_limelightinterface.hasValidTarget())
          {return true;}
    else  {return false;}
  } 
  else
  return false;

}
    //return turnError>-0.2 &&  turnError<0.2 && /*(rangeError-distanceOffset)>-0.6 &&  (rangeError-distanceOffset)<0.6 &&*/ s_limelightinterface.hasValidTarget();
  }

}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.NavXInterface;

public class driveDistance extends CommandBase {
  /**
   * Creates a new driveDistance.
   */
  private boolean isFirstRun = true;
  private final DriveTrain s_drivetrain;
  private final NavXInterface s_NavXInterface;
  public double targetDistance;
  double driveError;
  public driveDistance(DriveTrain driveTrain, NavXInterface NavXInterface, double targetDistance) {
    // Use addRequirements() here to declare subsystem dependencies.
    s_drivetrain=driveTrain;
    s_NavXInterface = NavXInterface;
    addRequirements(s_drivetrain);
    this.targetDistance = -targetDistance;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (isFirstRun){s_drivetrain.rampRate(1); s_NavXInterface.yewReset(); s_drivetrain.resetEncoderpos(); isFirstRun = false;}
    
    if (targetDistance<0) s_drivetrain.drive(-0.3 ,-1*( s_NavXInterface.getYaw()*0.035));
    else s_drivetrain.drive(+0.3 ,-1*( s_NavXInterface.getYaw()*0.035));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    s_drivetrain.stop();
    s_drivetrain.switchModes(IdleMode.kBrake);
    isFirstRun = true;
    s_drivetrain.resetEncoderpos();
    s_drivetrain.rampRate(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if (targetDistance>0){ return (s_drivetrain.encodersPos())<= -targetDistance;}
    else {return (s_drivetrain.encodersPos())>= -targetDistance;}
  }
}

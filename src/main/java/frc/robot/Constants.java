/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.revrobotics.ColorMatch;

import edu.wpi.first.wpilibj.util.Color;
/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final class DriveConstants {
        public static final int kLeftMotor1Port = 1;
        public static final int kLeftMotor2Port = 2;
        public static final int kRightMotor1Port = 3;
        public static final int kRightMotor2Port = 4;



        public static final int kEncoderCPR = 1024;
        public static final double kMotorToWheel = 1/10;
        public static final double kWheelDiameterCMs = 6;
        public static final double kEncoderDistancePerPulse =
            // Assumes the encoders are directly mounted on the wheel shafts
            (kWheelDiameterCMs * Math.PI) / (double) kEncoderCPR;
    }

    public static final class ColorConstants {
        public static final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
        public static final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
        public static final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
        public static final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);
    }

    public static final class SolenoidConstants {
        public static final int kBall1 = 0;
        public static final int kLock = 1;
    
    }
    public static final class BallConstants {
        public static final int kColorRotate = 0;
        public static final int kShooter1 = 5;
        public static final int kShooter2 = 6;
        public static final int kVerticalBelt = 14;
        public static final int kHorizontalBelt = 13;
        public static final int kBeltWheels = 12;
        public static final int kPickWheels = 11;
    }
    public static final class ElevatorConstants {
        public static final int kElevator1 = 7;
        public static final int kElevator2 = 8;
        public static final int kElevatorUp = 15;
    }

    public static final class ButtonConstants {
        //alon
        public static final int kRightShoulderButton = 6;
        public static final int kLeftShoulderButton = 5;
        public static final int kBButton = 2;
        public static final int kYButton = 4;
        public static final int kXButton = 3;
        public static final int kAButton = 1;
        public static final int kStartButton = 8;
        public static final int kbackButton = 7;
        public static final int kLeftTrigger = 2;
        public static final int kRightTrigger = 3;
        //hallel
        public static final int hRightShoulderButton = 6;
        public static final int hLeftShoulderButton = 5;
        public static final int hBButton = 7;
        public static final int hYButton = 8;
        public static final int hXButton = 9;
        public static final int hAButton = 10;
        public static final int hStartButton = 11;
        public static final int hSelectButton = 12;
        public static final int hRBButton = 7;
        public static final int hLeftDownButton = 8;
      

    }

    public static class SpeedToDistanceShoot {
        public static double kOffset = 0.15;
    }
}

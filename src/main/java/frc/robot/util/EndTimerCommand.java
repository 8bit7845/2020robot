/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.util;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

public class EndTimerCommand extends SequentialCommandGroup {
  /**
   * Creates a new EndTimerCommand.
   */

  public EndTimerCommand(Command toRun, double seconds) {
    // Use addRequirements() here to declare subsystem dependencies.
    super(new WaitCommand(seconds), toRun);
  }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.util;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.Trigger;

/**
 * Add your docs here.
 */
public class ShoulderButton extends Trigger {

  Joystick stick;
  int axis;
  double limit;
  public ShoulderButton(Joystick stick, int axis, double limit){
    this.stick = stick;
    this.axis = axis;
    this.limit = limit;
  }
  @Override
  public boolean get() {
    return stick.getRawAxis(axis) > limit;
  }
}

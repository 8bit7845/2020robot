/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.util;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants.SpeedToDistanceShoot;

/**
 * Add your docs here.
 */
public class manualSetShootSpeed {
    
    static double[][] table = new double[][] {
        {2.7, -0.88},
        {6.8, -0.87},
        {8.6, -0.9},
        {12.5, -0.85},
        {17, -0.91},
        {19, -0.95}, 
    };
    static double slope(int pointer) { 
        return (table[pointer+1][1] - table[pointer][1]) / (table[pointer+1][0] - table[pointer][0]); 
    } 

    static double pointOnFunc(double y, int pointer){
        return slope(pointer) * (y - table[pointer][0]) + table[pointer][1];
    }
    public static double get(double y) {
        if(y<-7.2) return -1;
        if (table[0][0] > y) return table[0][1];
        if (table[table.length-1][0] < y) return table[table.length-1][1];
        int pointer = 0;
        for(int i = 0; i < table.length; i++) {
            if (y >= table[i][0] && y < table[i+1][0]){
                pointer = i;
                break; 
            }
        }
        double destSpeed = pointOnFunc(y, pointer)+SpeedToDistanceShoot.kOffset;
        SmartDashboard.putNumber("shooting speed", destSpeed);
        return destSpeed;
    }
}

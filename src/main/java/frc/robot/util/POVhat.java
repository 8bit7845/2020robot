/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.util;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.Trigger;

/**
 * Allows the POV hat switch on the joystick to be represented like a group 
 * of 8 buttons.  Attach commands as easily as with buttons.
 * @author Dustin Maki, modifications by Yochanan Zidman
 */
public class POVhat // outer class
{     
    Joystick stick;       
    public POVtrigger Right;
    public POVtrigger UpRight;
    public POVtrigger Up;
    public POVtrigger UpLeft;
    public POVtrigger Left;
    public POVtrigger DownLeft;
    public POVtrigger Down;
    public POVtrigger DownRight;

    private static final int kRight = 90;
    private static final int kUpRight = 45;
    private static final int kUp = 0;
    private static final int kUpLeft = 315;
    private static final int kLeft = 270;
    private static final int kDownLeft = 225;
    private static final int kDown = 180;
    private static final int kDownRight = 135;
    
    public class POVtrigger extends Trigger // inner class
    {    
        private int angle;
        protected POVtrigger(int angle)// inner class constructor
        {        
            this.angle = angle;
        }

        public boolean get() // mandatory override from Trigger class
        {
            return (angle == stick.getPOV());
        }
    }   
    // begin outer class constructors
//    public POVhat()
//    {
//        this(Robot.oi.driverJoystick);          
//    }
    
    public POVhat(Joystick stick)
    {
        this.stick = stick;
        this.Right = new POVtrigger(kRight);
        this.UpRight = new POVtrigger(kUpRight);
        this.Up = new POVtrigger(kUp);
        this.UpLeft = new POVtrigger(kUpLeft);
        this.Left = new POVtrigger(kLeft);
        this.DownLeft = new POVtrigger(kDownLeft);
        this.Down = new POVtrigger(kDown);
        this.DownRight = new POVtrigger(kDownRight);
    }   
    // end outer class constructors
}

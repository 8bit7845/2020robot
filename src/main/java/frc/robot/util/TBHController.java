/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.util;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.SpeedController;

/**
 * Implements a take-back-half speed control system. Creates a separate thread
 * that frequently schedules speed control.
 *
 * <p>
 * For information, see:
 * <a href="http://www.chiefdelphi.com/forums/showthread.php?threadid=105965">http://www.chiefdelphi.com/forums/showthread.php?threadid=105965</a>
 * <a href="http://www.chiefdelphi.com/media/papers/2674?">http://www.chiefdelphi.com/media/papers/2674?</a>
 * </p>
 * 
 * 
 * <h4>Usage</h4>
 * <p>
 * Have a DoubleSupplier, which is linked to a sensor that measures the actual
 * speed of whatever is being controlled.
 * </p>
 * <p>
 * Have a SpeedController that controls the speed of whatever is being controlled
 * </p>
 * 
 * <pre>
 * //construct
 * ThreadedSpeedController controller = new TakeBackHalfSpeedController(source, controller);
 * //Set the target speed, in RPM
 * controller.setTargetRpm(2000);
 * 
 * //Start the seperate thread that does speed control
 * controller.start();
 * //Set the controller to enabled, so that it will start controlling the motor
 * controller.enable();
 * 
 * //...
 * 
 * //Disable the controller
 * controller.disable();
 * 
 * </pre>
 *
 * @author Sam Crow
 */
public class TBHController {

    /**
     * The target speed, in RPM
     */
    private volatile double targetRpm = 0;
    //Control data
    
    /**
     * The ratio (0-1) of motor power that is being applied
     */
    private double motorPower = 0;
    
    /**
     * Gain (G)
     */
    private double gain = 1E-5;
    /**
     * Tolerance
     */
    private double tolerance = 300;
    /**
     * The error encountered during the last loop
     */
    private double lastError = 10;
    /**
     * The take-back-half variable (b)
     */
    private double tbh = 0;
    
    /**
     * The approximate speed of the shooter wheels, in RPM,
     * at full motor power. This is used for spinup optimization.
     */
    private double kMaxRpm = 6000;

    private double measurement = 0;

    /**
     * Constructor
     *
     * @param source The source to use to get the speed
     * @param controller The speed controller to control
     */
    public TBHController(double maxRpm, double gain, double tolerance) {
        maxRpm = kMaxRpm;
        this.gain = gain;
        this.tolerance = tolerance;
    }

    public TBHController() {
    }

    public void reset(){
        lastError = 0;
    }

    public double calculate(double measurement, double setpoint) {
        setTargetRpm(setpoint);
        return calculate(measurement);
    }

    public double calculate(double measurement) {
        this.measurement= measurement;
        double error = targetRpm - measurement;
		
        motorPower += gain * error;

        motorPower = clamp(motorPower);

        //If the error has changed in sign since the last processing
        if (isPositive(lastError) != isPositive(error)) {
            motorPower = 0.5 * (motorPower + tbh);
            tbh = motorPower;

            lastError = error;
        }

        return motorPower;

    }

    public boolean isOnTarget() {
        double difference = measurement - targetRpm;

        return Math.abs(difference) < 100;
    }

    /**
     * @see ThreadedSpeedController#setTargetRpm(double)
     */
    public synchronized void setTargetRpm(double newRpm) {
        
        //Set up values for optimized spinup to the target
        if(targetRpm < newRpm) {
            lastError = 1;
        }
        else if(targetRpm > newRpm) {
            lastError = -1;
        }
        tbh = (2 * (newRpm / kMaxRpm)) - 1;
        
        targetRpm = newRpm;
    }

    /**
     * Clamp a value to within +/- 1, for use with speed controllers
     *
     * @param input The value to clamp
     * @return the clamped value
     */
    private static double clamp(double input) {
        if (input > 1) {
            return 1;
        }
        if (input < -1) {
            return -1;
        }
        return input;
    }

    /**
     * Determine if a value is positive
     *
     * @param input
     * @return true if positive
     */
    private static boolean isPositive(double input) {
        return input > 0;
    }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.util;

/**
 * Add your docs here.
 */
public class getRPMForDistance {
    static double[][] table = new double[][] {
        {3, 500},
        {-9.00, 5000},
        {-10.00, 2000},
        {-17.00, 1000},
        };
    
    static double slope(double y2, double y1, double x2, double x1) { 
        return (y2 - y1) / (x2 - x1); 
    } 

    static double pointOnFunc(double m, double originalY, double originalRPM, double y){
        return m * (y - originalY) + originalRPM;
    }
    public double get(double y) {
        if (table[0][0] < y) return table[0][1];
        if (table[table.length-1][0] > y) return table[table.length-1][1];
        int pointer = 0;
        for(int i = 0; i < table.length; i++) {
            if (y <= table[i][0] && y > table[i+1][0]){
                pointer = i;
                break; 
            }
        }
        double m = slope(table[pointer+1][1], table[pointer][1], table[pointer+1][0], table[pointer][0]); 
        return pointOnFunc(m, table[pointer][0], table[pointer][1], y);
    }
}

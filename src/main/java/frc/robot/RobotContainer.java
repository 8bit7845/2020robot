/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.ButtonConstants;
import frc.robot.Constants.SpeedToDistanceShoot;
import frc.robot.commands.AutoAlignTurn;
import frc.robot.commands.AutoShoot;
import frc.robot.commands.AutoTurn;
import frc.robot.commands.Brake;
import frc.robot.commands.DriveTank;
import frc.robot.commands.RPMShoot;
import frc.robot.commands.ShootTBH;
import frc.robot.commands.driveDistance;
import frc.robot.commands.smartConveyor;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Elevator;
import frc.robot.subsystems.LimelightInterface;
import frc.robot.subsystems.NavXInterface;
import frc.robot.subsystems.Solenoids;
import frc.robot.util.POVhat;
import frc.robot.util.ShoulderButton;
import frc.robot.util.getRPMForDistance;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  
  private final Joystick Xbox = new Joystick(0);
  private final Joystick mainLogiDS = new Joystick(0);
  private final Joystick LogiDS = new Joystick(1);

  private final POVhat Xboxpov = new POVhat(Xbox);
  private final POVhat LogiDSpov = new POVhat(mainLogiDS);

  // The robot's subsystems are defined here...

  private final DriveTrain s_drivetrain = new DriveTrain();

  private final LimelightInterface s_limelightinterface = new LimelightInterface();

  private final BallIntake s_ballintake = new BallIntake();

  private final getRPMForDistance s_getRPMForDistance = new getRPMForDistance();

  private final Elevator s_elevator = new Elevator();

  private final NavXInterface s_navxinterface = new NavXInterface();

  private final Solenoids s_solenoids = new Solenoids();
  
  private final SendableChooser<Command> autochooser = new SendableChooser<>();
  
  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    s_drivetrain.setDefaultCommand(
      new DriveTank(
          s_drivetrain,
          () -> -Xbox.getRawAxis(1),
          () -> -Xbox.getRawAxis(4),
          () -> Math.max(1-Xbox.getRawAxis(2), 0.2)));
    
      new Thread(() -> {
        CameraServer.getInstance().startAutomaticCapture(0);
      }).start();
      

    Compressor c = new Compressor(0);
    c.setClosedLoopControl(false);
          
    // Add commands to the autonomous command chooser
    autochooser.addOption("Shoot balls and drive backwards", c_auto_shootAndBack());
    autochooser.addOption("Collect 2 balls then shoot 5 balls", c_auto_hatchCollect2from3Shoot());

    // Put the chooser on the dashboard
    SmartDashboard.putData("Autonomous", autochooser);    

    // Configure the button bindings
    configureButtonBindings();
  }

  

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    //Alon
    //Button that halves the maximum speed of the DriveTrain
      new JoystickButton(Xbox, ButtonConstants.kLeftShoulderButton) //max output
          .whenPressed(() -> s_drivetrain.setMaxOutput(0.4))          // this reduses the max output for driving
          .whenReleased(() -> s_drivetrain.setMaxOutput(1));
          
      new JoystickButton(Xbox, ButtonConstants.kRightShoulderButton) //break
          .whileHeld(new Brake(s_drivetrain));
          
      new ShoulderButton(Xbox, ButtonConstants.kLeftTrigger, 0.5) // smartConveyor = Left Trigger
          .whileActiveContinuous(new smartConveyor(s_ballintake, s_drivetrain, s_solenoids));

      new ShoulderButton(Xbox, ButtonConstants.kRightTrigger, 0.5) // align, drive and shoot
          .whileActiveOnce(new SequentialCommandGroup(
            //new InstantCommand(()->s_ballintake.openLoopShoot(0.5)),
            new AutoAlignTurn(s_ballintake, s_limelightinterface, s_drivetrain, 0), //turn and drive distance
            new RPMShoot(s_limelightinterface, s_ballintake, s_getRPMForDistance))) //speed and shoot
          .whenInactive(()->s_ballintake.stop()); //stops all

      new JoystickButton(Xbox, ButtonConstants.kYButton) // drive back and shoot
          .whileActiveOnce(new SequentialCommandGroup(
            //new InstantCommand(()->s_ballintake.shootRPM(6000)),
            new driveDistance(s_drivetrain, s_navxinterface, -30), //turn and drive distance
            new RPMShoot(s_limelightinterface, s_ballintake, s_getRPMForDistance))) //speed and shoot
          .whenInactive(()->s_ballintake.stop()); //stops all

      new JoystickButton(Xbox, ButtonConstants.kAButton)       // shoot = 1 A
          .whenPressed(() -> s_ballintake.openLoopShoot(1))
          .whenReleased(() -> s_ballintake.stop());

      new JoystickButton(Xbox, ButtonConstants.kbackButton)         //out the ball  = 7 back
          .whenPressed(() -> s_ballintake.out())
          .whenReleased(() -> s_ballintake.stop());

      new JoystickButton(Xbox, ButtonConstants.kStartButton)         //in the ball = 8 start
          .whenPressed(() -> s_ballintake.in())
          .whenReleased(()-> s_ballintake.stop());



/***********************************************************************************************************************
*                      Hallel                                                                                          *
***********************************************************************************************************************/

new JoystickButton(LogiDS, ButtonConstants.kBButton)         //out the ball  = 2 B
    .whenPressed(() -> s_ballintake.out())
    .whenReleased(() -> s_ballintake.stop());

  new JoystickButton(LogiDS, ButtonConstants.kXButton)         //in the ball = 3 X
      .whenPressed(() -> s_ballintake.in())
      .whenReleased(()-> s_ballintake.stop());

  /*new JoystickButton(LogiDS, ButtonConstants.kAButton)        //ball intake = 1 A
      .whenPressed(() -> {
        s_solenoids.openBallCollector();
        s_ballintake.BallCollector();
      })
      .whenReleased(() -> {
        s_solenoids.closeBallCollector();
        s_ballintake.BallCollectorStop();
      });*/

  new JoystickButton(LogiDS, ButtonConstants.kYButton)       // shoot = 4 Y
      .whenPressed(() -> s_ballintake.openLoopShoot(1))
      .whenReleased(() -> s_ballintake.stop());



  new JoystickButton(LogiDS, ButtonConstants.kLeftShoulderButton)
      /*.whenHeld(
        new SequentialCommandGroup( 
          new ShootTBH(s_limelightinterface, s_ballintake),
          new InstantCommand(() -> s_ballintake.in())))*/
      .whenHeld(new ShootTBH(s_limelightinterface, s_ballintake))
      .whenReleased(() -> s_ballintake.stop());
    Xboxpov.Up.whenActive(() -> { 
      SpeedToDistanceShoot.kOffset += 0.05;
      SmartDashboard.putNumber("ShootOffset", SpeedToDistanceShoot.kOffset);
    });
    Xboxpov.Down.whenActive(() -> { 
      SpeedToDistanceShoot.kOffset -= 0.05;
      SmartDashboard.putNumber("ShootOffset", SpeedToDistanceShoot.kOffset);
    });


      
         new JoystickButton(mainLogiDS, 2)
          .whileHeld(() -> s_ballintake.in());
        LogiDSpov.Up.whenActive(new AutoTurn(s_navxinterface, s_drivetrain, 180));
        LogiDSpov.Right.whenActive(new AutoTurn(s_navxinterface, s_drivetrain, 90));
        LogiDSpov.Left.whenActive(new AutoTurn(s_navxinterface, s_drivetrain, -90));
        LogiDSpov.Down.whenActive(new AutoTurn(s_navxinterface, s_drivetrain, -180));
  }



  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   * 
   * 
   * shoot and drive back
   */

  public Command c_auto_shootAndBack() {
    // An ExampleCommand will run in autonomous
    //return new SequentialCommandGroup(new AutoAlignTurn(s_limelightinterface, s_drivetrain, false).withTimeout(3), new AutoShoot(s_limelightinterface,s_ballintake, -0.9).withTimeout(3), new AutoAlignTurn(s_limelightinterface, s_drivetrain, true).withTimeout(4));
    return new SequentialCommandGroup(
      new InstantCommand(()->s_ballintake.openLoopShoot(0.5)),
      new AutoAlignTurn(s_ballintake, s_limelightinterface, s_drivetrain, -0), 
      new RPMShoot(s_limelightinterface, s_ballintake, s_getRPMForDistance).withTimeout(4)
    );
      //new driveDistance(s_drivetrain, s_navxinterface, -200));
  }

    /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   * 
   * 
   * colect 2 from the 3 balls next to the hatch and shoot 5
   */

  public Command c_auto_hatchCollect2from3Shoot() {
    // An ExampleCommand will run in autonomous
    //return new SequentialCommandGroup(new AutoAlignTurn(s_limelightinterface, s_drivetrain, false).withTimeout(3), new AutoShoot(s_limelightinterface,s_ballintake, -0.9).withTimeout(3), new AutoAlignTurn(s_limelightinterface, s_drivetrain, true).withTimeout(4));
    return new SequentialCommandGroup(
      new driveDistance(s_drivetrain, s_navxinterface, 170),
      new ParallelDeadlineGroup(new driveDistance(s_drivetrain, s_navxinterface, 250), new smartConveyor(s_ballintake, s_drivetrain, s_solenoids)),
      new smartConveyor(s_ballintake, s_drivetrain, s_solenoids).withTimeout(2),
      new AutoTurn(s_navxinterface, s_drivetrain, 170),
      new driveDistance(s_drivetrain, s_navxinterface, 400),
      new driveDistance(s_drivetrain, s_navxinterface, 200).withInterrupt(() -> s_limelightinterface.hasValidTarget()),
      new AutoAlignTurn(s_ballintake, s_limelightinterface, s_drivetrain, 0),
      new RPMShoot(s_limelightinterface, s_ballintake, s_getRPMForDistance).withTimeout(4));
      //new AutoTurn(s_navxinterface, s_drivetrain, 150);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  
  public Command getAutonomousCommand() {
    return autochooser.getSelected();
  }

  public Command getTestCommand() {
    // An ExampleCommand will run in autonomous
    //return new SequentialCommandGroup(new InstantCommand(() -> { s_ballintake.test(); s_drivetrain.test(); }));
    return new InstantCommand(() -> s_ballintake.openLoopShoot(0.5));
  }

}
